
#pragma once

#include <ostream>
#include <boost/format.hpp>

#include <glm/vec3.hpp>

std::ostream& operator<<(std::ostream& os, const glm::vec3 &p) {
	return os << boost::format("{%1$+9d, %2$+9d, %3$+9d}") % p.x % p.y % p.z;
}
