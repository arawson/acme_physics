
#include <iostream>
#include <ctime>
#include <boost/format.hpp>

#include "writers.hpp"
#include "src/shape.hpp"

using std::cin;
using std::cout;
using std::endl;

using glm::vec3;

using boost::format;

int main(int argc, char *argv[])
{
	cout << "meson my boson" << endl;
	cout << "boson my meson" << endl;
	//heaps beached az

	vec3 n = {0, 0, 1};
	plane pl = {n, 0};

	for (float x = -10; x <= +10; x += 1) {
	for (float z = -1; z <= 1; z += 0.25) {
		vec3 p = {x, 0, z};
		cout << "Intersects? " << pl.is_point_within(p)
			<< " Point " << p << " is " << pl.get_distance(p) << " units from plane." << endl;
	}
	}

	cout << endl;
}
