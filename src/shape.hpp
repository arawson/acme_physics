
#pragma once

#include <glm/vec3.hpp>
#include <glm/geometric.hpp>

enum class shape_type {
	RAY,
	CUBE, //
	PLANE, //plane equation
	SPHERE, //center and radius
	HEIGHTMAP, //hmmm
	CAPSULE //
};

class shape {
public:
	virtual ~shape() {}
};

class collision_checker {
public:
	virtual ~collision_checker() {}

	virtual bool operator()(const shape* s1, const shape* s2) const = 0;
};

class narrow_phase : public collision_checker {
public:
	~narrow_phase();

	bool operator()(const shape* s1, const shape* s2) const;
};

class sphere : public shape {
public:
	glm::vec3 p;
	float r;

	virtual ~sphere() {}

	bool is_point_within(const glm::vec3 p) const;

	float get_distance(const glm::vec3 p) const;
};

inline bool sphere::is_point_within(const glm::vec3 p) const {
	return get_distance(p) <= 0;
}

inline float sphere::get_distance(const glm::vec3 p) const {
	return glm::distance(this->p, p) - r;
}

class plane : public shape {
public:
	//hessian normal form
	glm::vec3 n;
	float p;

	plane(const glm::vec3 &n, const float& p) : n(n), p(p) {}

	~plane() {}

	bool is_point_within(const glm::vec3 p) const;

	float get_distance(const glm::vec3 p) const;
};

inline bool plane::is_point_within(const glm::vec3 p) const {
	return get_distance(p) >= 0;
}

inline float plane::get_distance(const glm::vec3 p) const {
	return glm::dot(n, p) + this->p;
}
