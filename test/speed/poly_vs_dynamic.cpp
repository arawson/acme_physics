
#include <cstdlib>
#include <random>
#include <iostream>
#include <ctime>

#include "poly_vs_dynamic.hpp"

CPPUNIT_TEST_SUITE_REGISTRATION(poly_vs_dynamic_test);

using namespace std;

enum class poly_state {
	A, B, C
};

class poly {
public:
	virtual ~poly() {}
	virtual poly_state state() const = 0;
};

class poly_a : public poly {
public:
	~poly_a() {}
	poly_state state() const {
		return poly_state::A;
	}
};

class poly_b : public poly {
public:
	~poly_b() {}
	poly_state state() const {
		return poly_state::B;
	}
};

class poly_c : public poly {
public:
	~poly_c() {}
	poly_state state() const {
		return poly_state::C;
	}
};

class dynamic {
public:
	virtual ~dynamic() {}
};

class dynamic_a : public dynamic {
public:
	~dynamic_a() {}
};

class dynamic_b : public dynamic {
public:
	~dynamic_b() {}
};

class dynamic_c : public dynamic {
public:
	~dynamic_c() {}
};

void poly_vs_dynamic_test::setUp() {
	mt19937_64 rando;

	auto const lim_a = mt19937_64::max() / 4;
	auto const lim_b = mt19937_64::max() / 2;

	for (size_t i = 0; i < iterations; i++) {
		auto const r = rando();
		if (r <= lim_a) {
			polies.push_back(new poly_a());
			dynamics.push_back(new dynamic_a());
		} else if (r <= lim_b) {
			polies.push_back(new poly_b());
			dynamics.push_back(new dynamic_b());
		} else {
			polies.push_back(new poly_c());
			dynamics.push_back(new dynamic_c());
		}
	}
}

void poly_vs_dynamic_test::tearDown() {
	for (auto p : polies) {
		delete p;
	}
	for (auto p : dynamics) {
		delete p;
	}
}

void poly_vs_dynamic_test::test_polymorphism() {
	size_t a = 0, b = 0, c = 0;

	auto start = clock();

	for (auto p : polies) {
		switch(p->state()) {
			case poly_state::A:
				a += 1;
				break;
			case poly_state::B:
				b += 1;
				break;
			case poly_state::C:
				c += 1;
				break;
			default:
				break;
		}
	}

	auto end = clock();

	cout << "A = " << a << ", B = " << b << ", C = " << c << ", T = "
		<< (end - start) / ((double)CLOCKS_PER_SEC / 1000.0) << "ms" << endl;

	CPPUNIT_ASSERT_ASSERTION_PASS();
}

void poly_vs_dynamic_test::test_dynamic_casts() {
	size_t a = 0, b = 0, c = 0;

	auto start = clock();

	for (auto p : dynamics) {
		if (auto pa = dynamic_cast<dynamic_a*>(p)) {
			a += 1;
		} else if (auto pb = dynamic_cast<dynamic_b*>(p)) {
			b += 1;
		} else if (auto pc = dynamic_cast<dynamic_c*>(p)) {
			c += 1;
		}
	}

	auto end = clock();

	cout << "A = " << a << ", B = " << b << ", C = " << c << ", T = "
		<< (end - start) / ((double)CLOCKS_PER_SEC / 1000.0) << "ms" << endl;
	
	CPPUNIT_ASSERT_ASSERTION_PASS();
}
