
#pragma once

#include <cppunit/extensions/HelperMacros.h>
#include <vector>

class poly;
class dynamic;

class poly_vs_dynamic_test : public CppUnit::TestFixture {
	CPPUNIT_TEST_SUITE(poly_vs_dynamic_test);
	CPPUNIT_TEST(test_polymorphism);
	CPPUNIT_TEST(test_dynamic_casts);
	CPPUNIT_TEST_SUITE_END();

	std::vector<poly*> polies;
	std::vector<dynamic*> dynamics;

	const size_t iterations = 1e6;

public:
	void setUp();
	void tearDown();

	void test_polymorphism();
	void test_dynamic_casts();
};
