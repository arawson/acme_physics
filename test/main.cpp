
#include <cppunit/CompilerOutputter.h>
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

int main(int argc, char* argv[]) {
	//get the top level suite from the registry
	CppUnit::Test *suite = CppUnit::TestFactoryRegistry::getRegistry().makeTest();

	//adds the test to the list of tests to run
	CppUnit::TextUi::TestRunner runner;
	runner.addTest(suite);

	//change the default outputter to a compiler error format outputer
	runner.setOutputter(new CppUnit::CompilerOutputter(&runner.result(), std::cerr));

	//run the tests
	bool wasSuccessful = runner.run();

	//return error code 1 if tests failed
	return wasSuccessful ? 0 : 1;
}

